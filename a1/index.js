//1
fetch('http://jsonplaceholder.typicode.com/todos')
.then((response)=>response.json())
.then((data)=>{
	let list = data.map((todo)=>{
		return todo.title;
	});
	console.log(list);
});

//2
fetch('http://jsonplaceholder.typicode.com/todos/1')
.then((response)=>response.json())
.then((data)=>console.log(`The item "${data.title} on the list has a status of ${data.completed}`));

//3
fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title:'Created To Do List Item',
		completed:false,
		userId:1
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data))

//4
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title:'Updated To Do List Item',
		description:'To update the my to do list with a different data structure',
		status: 'Pending',
		dateCompleted: 'Pending',
		userID: 1
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data))

//5
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		status:"Complete",
		dateCompleted:"07/09/21"
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data))

//6
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE',
})
.then((response)=>response.json())
.then((data)=>console.log(data))